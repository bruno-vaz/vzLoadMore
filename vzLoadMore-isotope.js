$(function(){
  setTimeout(function(){
    startIsotope();
  },250);
})
//Infinite scroll
$(function(){
  startInfiniteScroll();
})
function startInfiniteScroll(){
  $('.isotope-grid').infinitescroll({
    debug: false,
    navSelector: "#page_nav>a",
    nextSelector: "#page_nav>a",
    itemSelector: ".isotope-grid .isotope-item",
    loading: {
      img: null,
      msgText: "",
      finishedMsg: "",
    },
    errorCallback: function(){
      $('#page_nav>a').hide();
      $('#page_nav>.no-more').show();
    }
  },function(newElems){
    $('#page_nav>a').show();
    $(this).isotope( 'appended', newElems);
    $(document).trigger("elemloaded");
  });
  $(window).unbind('.infscr');
}
$(document).on('click','#page_nav>a',function(e){
  e.preventDefault();
  $('.isotope-grid').infinitescroll('retrieve');
  return false;
});
